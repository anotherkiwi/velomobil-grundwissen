#!/bin/sh

# extract the content, add a blank page in front
pdftk Velomobil-Grundwissen_print.pdf cat 4 2-r2 output content.pdf

# extract the two cover pages
pdftk Velomobil-Grundwissen_print.pdf cat 1 output frontcover.pdf
pdftk Velomobil-Grundwissen_print.pdf cat r1 output backcover.pdf
