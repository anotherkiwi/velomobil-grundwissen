#!/usr/bin/perl -l

$htmlfile = pop @ARGV;


# * read production list of intercitybike and velomobiel
#   (compiled from: https://www.velomobiel.nl/orderboek/, https://www.intercitybike.nl/en/orderboek/)
# * columns: short name, long name, date, number of velomobiles
# * replace "Quattrovelo" by "Quatrevelo"
# * remove entries after 2019 because they are already on the velomobile World website
# * do not remove DF, because it is not contained in the Velomobile World orderlist
push(@a, $_) for grep { $_->[0] =~ /^DF/ or $_->[2] !~ /202\d-/ } map { $_->[0] =~ s/Quattrovelo/Quatrevelo/; $_ } map { [ split /\t/ ] } <>;

# read input HTML file (download from: https://www.velomobileworld.com/de/auftraege/)
@ARGV = $htmlfile;
$/ = undef;
$html = <>;

# * search for all velomobiles (name in bl-itemName, delivery date in bl-expectedDeliveryTime)
# * extract short name, full name and date
# * set 4th column to 1 (each line is one single velomobile)
# * fix/reformat some of the short names
while ($html =~ /<div class="bl-itemName">\s*((SL|GT|Snoek|Quat?tr[oe]velo|Quest(?: XS)?|Strada|DF(?:XL)?|Alpha [79]|M 9|Bulk)?[^<>]*)<.+?<div class="bl-expectedDeliveryTime">([^<>]+)</g) {
	@b = ($2, $1, $3, 1);
	$b[0] =~ s/^GT|SL$/Milan $&/;
	$b[0] =~ s/^DFXL$/DF XL/;
	$b[0] =~ s/^Quat?trovelo$/Quatrevelo/;
	$b[0] =~ s/^Bulk$/Bülk/;
	push(@a, [@b]);
}

# * extract those velomobiles that have a date (and not "TBD")
# * sort them according to date
# * cumulated number of velomobiles: add value of 4th column
# * print the output
print join "\t", @$_ for
	map {
		$vm{$_->[0]} += $_->[3];
		[@$_[0..2], $vm{$_->[0]}]
	}
	sort { $a->[2] cmp $b->[2] }
	grep { $_->[2] ne "TBD" } @a;

