var dataDef = {
// dimensions
	'weight'                 : { title : 'Weight'                   , type : 'range' , unit : 'kg' , step : .5 , info : 'ready to drive from' },
	'length'                 : { title : 'Length'                     , type : 'range' , unit : 'cm' },
	'width'                  : { title : 'Width'                    , type : 'range' , unit : 'cm' },
	'height'                 : { title : 'Height'                      , type : 'range' , unit : 'cm' },
	'groundclearance'        : { title : 'Ground clearance'             , type : 'range' , unit : 'cm' , info : 'Driving over an obstacle on level ground'},
	'trackwidth'             : { title : 'Track width'                , type : 'range' , unit : 'cm' },
	'wheelbase'              : { title : 'Wheelbase'                  , type : 'range' , unit : 'cm' },
	'minlengthdriver'        : { title : 'min. Height driver'         , type : 'range' , unit : 'cm' },
	'maxlengthdriver'        : { title : 'max. Height driver'         , type : 'range' , unit : 'cm' },
	'minxseam'               : { title : 'min. X-Seam'               , type : 'range' , unit : 'cm' },
	'maxxseam'               : { title : 'max. X-Seam'               , type : 'range' , unit : 'cm' },
	'maxshoulderwidth'       : { title : 'max. Shoulder width'       , type : 'range' , unit : 'cm' , step : .5 , info : 'Inside width of the VM in the shoulder area' },
	'maxthighwidth'          : { title : 'max. thigh width' , type : 'range' , unit : 'cm' },
	'maxshoesize'            : { title : 'max. Shoe size'           , type : 'range' , info : 'Largest shoe size for a tall rider who has ever ridden regularly in this VM, with simple adjustments if necessary (e.g. extended foot holes).' },
	'driversopeningwidth'    : { title : 'Entry opening width'      , type : 'range' , unit : 'cm' , info : 'at the widest point' },
	'driversopeninglength'   : { title : 'Entry opening length'       , type : 'range' , unit : 'cm' , info : 'at the longest point' },
	'luggagecapacity'        : { title : 'Luggage capacity'               , type : 'range' , unit : 'l' , info : 'Number of 1 litre packages you can load without them interfering with driving' },
	'maxfrontwheelwidth'     : { title : 'max. front wheel width'   , type : 'range' , unit : 'mm' },
	'maxrearwheeldiameter'   : { title : 'max. Rear wheel diameter' , type : 'range' , unit : 'mm' , info : 'Rim dimension + 2 x tyre lateral diameter, e.g. for 40-622 -> 622 + (2 x 40) = 702' },
	'turningcircle28mm'      : { title : 'Turning circle 28mm tyre'       , type : 'range' , unit : 'm' , step : .5 , info : 'between 2 walls' },
	'turningcircle40mm'      : { title : 'Turning circle 40mm tyre'       , type : 'range' , unit : 'm' , step : .5 , info : 'between 2 walls' },
	'mindevelopment'         : { title : 'min. Development'           , type : 'range' , unit : 'cm' , info : 'Calculated from largest chainring, smallest sprocket and drive wheel dimension' },
	'maxdevelopment'         : { title : 'max. Development'           , type : 'range' , unit : 'cm' , info : 'Calculated from largest chainring, smallest sprocket and drive wheel dimension' },
	'comfortablespeed'       : { title : 'Comfortable speed'   , type : 'radio' , values : { 'low' : '< 32 km/h' , 'medium' : '32-36 km/h' , 'high' : '37-43 km/h' , 'speed' : '> 43 km/h' } , info : 'Permanent cruising speed after acceleration on good, level asphalt' },

// configuration
	'steering'               : { title : 'Steering'                   , type : 'checkbox' , values : { 'tiller' : 'Tiller' , 'sidestick' : 'Tank' }},
	'frontwheels'            : { title : 'Front wheel well'               , type : 'radio' , values : { 'open' : 'open' , 'covered' : 'closed' }},
	'hood'                   : { title : 'Hood'                     , type : 'checkbox' , values : { 'removablehood' : 'removable hood' , 'foldinghood' : 'folding hood' , 'foamcover' : 'foam cover' , 'tarp' : 'tarp' }},
	'hoodtransport'          : { title : 'Hood transport'     , type : 'checkbox' , values : { 'hoodyes' : 'hood yes' , 'hoodno' : 'hood no' , 'foamcoveryes' : 'foam cover yes' , 'foamcoverno' : 'foam cover no' }},
	'openchain'              : { title : 'chain line'                     , type : 'radio' , values : { 'open' : 'open' , 'covered' : 'covered' } , info : 'behind the first idler' },
	'maintenance'            : { title : 'maintenance hatch'               , type : 'checkbox' , values : { 'maintenanchole' : 'Maintenance hatch' , 'removablefront' : 'removable front', 'removableend' : 'removable rear' , 'openend' : 'open rear' }},
	'motorizable'            : { title : 'Possible motor position'    , type : 'checkbox' , values : { 'hub' : 'hub' , 'bottombracket' : 'bottom bracket' , 'intermediate' : 'intermediate' }},

// driving character
	'overallcharacter'       : { title : 'Overall character'             , type : 'checkbox' , values : { 'city' : 'city' , 'travel' : 'travel' , 'race' : 'race'} , 'info' : 'city: agile, good all-round visibility. Travel: runs with luggage on long distances. Racing: for sporty laps or on the racetrack. Can be configured differently for each purpose.' },
	'sidewindsensitivity'    : { title : 'Side wind sensitivity', type : 'radio' , values : { 'high' : 'high' , 'medium' : 'medium' , 'low' : 'low'}},
	'curvestability'         : { title : 'cornering stability'          , type : 'radio' , values : { 'low' : 'low' , 'medium' : 'medium' , 'high' : 'high'}},

// commercial information
	'pricefrom'              : { title : 'Pice from'                  , type : 'range' , unit : '€' },
	'builtfrom'              : { title : 'built from'                 , type : 'range' , enrich : function(vmData) { if (typeof vmData.builtfrom !== 'undefined' && typeof vmData.builtuntil === 'undefined' && vmData.buyable !== 'no') { vmData.buyable = 'yes' } else if (typeof vmData.builtuntil !== 'undefined' || vmData.buyable !== 'yes') { vmData.buyable = 'no' } } },
	'builtuntil'             : { title : 'built until'                , type : 'range' },
	'buyable'                : { title : 'buyable now'           , type : 'radio' , values : { 'yes' : 'yes' }},
	'website'                : { title : 'web site'       , type : 'link' },
};
