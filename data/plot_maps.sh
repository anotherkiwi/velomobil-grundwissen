#!/bin/sh

# Region: W, S, E, N
REGION='-R3/45.2/19/55.8+r'

# Map projection: Lambert, around 10E/50N, with parallels 45N and 55N
PROJ='-JL10/50/45/55/16c'

# Data directory
DATADIR="$(dirname "$0")"


plot_land_sea() {
	# Plot coastlines, lakes and borders:
	# -R: region
	# -J: map projection
	# -D: resolution (crude, low, intermediate, high, full)
	# -A: min. feature size [km^2] (e.g. lakes), min level (0 = sea), max level (2 = lakes)
	# -S, -G: colors for water/land
	# -N: draw state boundaries
	# colors: see https://docs.generic-mapping-tools.org/latest/_images/GMT_RGBchart.png
	gmt coast "$REGION" "$PROJ" -Di -Sroyalblue4 -N1/thinner "$@"
}


plot_velomobile_drivers() {
	# data:
	# * from rijderslijst_velomobiel_intercitybike_2020-02-29.csv using get_rijderslijst_coordinates.pl
	# * from vmforum_members.csv using awk '{ OFS = FS = "\t"; $1 = 0; $2 = ""; NF = 9; print; }' vmforum_members.csv > vmforum_members_anonymous.csv
	perl -F'\t' -ale '
		# first input file: consider only users (not dealers), round coordinates to 0.01°, save number
		!$f and $F[3] eq "user" and $m{sprintf "%.2f,%.2f", @F[0,1]} += $F[2];

		# second input file: only lines with coordinates and velomobile, round coordinates to 0.01°, save number
		$f and $F[3] and $F[4] and $F[6] and $m{sprintf "%.2f,%.2f", @F[4,3]}++;

		# increase input file number
		$f++ if eof;

		END {
			# write result: coordinates and square root of number, sorted by descending number
			print join "\t", split(/,/), sqrt($m{$_}) for sort { $m{$b} <=> $m{$a} } keys %m;
		}
	' "$DATADIR/rijderslijst_velomobiel_intercitybike_2020-02-29_coordinates.gmt" "$DATADIR/vmforum_members_anonymous_2022-08-31.csv" |
	# Plot circles:
	# -Sc: circle, with diameter, or from the data
	# -G: background color, or from the file headers ("> -Gwhite")
	# -W: plot outline
	# -i0,1,2: use columns 1, 2 and 3; 2+s0.1: multiply value of 3rd column by 0.1; t: keep non-numerical column
	gmt plot "$REGION" "$PROJ" -Sc -i0,1,2+s0.1,t -Gwhite -W --PROJ_LENGTH_UNIT=c
}


plot_velomobile_dealers() {
	# source: https://www.google.com/maps/d/viewer?hl=de&mid=1Mr5RVj9MMaFSy7xnXUbZmODqEAZDI4n1

	# Extract coordinates and labels from KML file,
	# and write a CSV table to a temporary file:
	TEMP=$(mktemp)
	sed -n -e '
		# if line contains a name tag:
		/<name>/ {
			# extract name
			s!.*<name>\(.*\)</name>.*!\1!;

			# remove CDATA tags, if present
			s/<!\[CDATA\[\(.*\)\]\]>/\1/;

			# shorten names:
			# * remove everything after space and slash or dash
			# * remove everything after comma
			# * remove company type and everything afterwards
			# * combine all Dronten companies in one label
			s! [/-].*\|,.*!!;
			s! \(GmbH\|UG\|Inh\).*!!;
			s/\(Beyss\).*/\1/;
			s/Velomobiel[^ ]*$/Velomobiel, InterCityBike, Alligt/;

			# push on stack, and go to next line
			h;
			b;
		}

		# coordinates: write them tab separated
		s/.*[^0-9.-]\([0-9.-]\+\),\([0-9.-]\+\),0.*/\1\t\2\t/;

		# if substitution did not work, go to next line
		T;

		# append label from stack and remove linebreak
		G;
		s!\n!!g;

		# remove some entries, and print
		/R3\|Intercity\|Gazelle\|[^,][^ ]Alligt\|LEITRA DK/b;
		p;
		' "$DATADIR/Velomobilhändler und -hersteller.kml" |
		tee "$TEMP"

	# Write labels:
	# -F: font
	#   * +f6p: size 6p
	#   * +jML: coordinates are middle/left of label
	# -D: offset x/y to coordinates
	# -G: background color
	gmt text "$REGION" "$PROJ" -F+f6p+jML -D0.1c/0c -Glightgrey < "$TEMP"

	# Plot circles (afterwards, so they are on top of labels when overlapping):
	# -Sc: circle, with diameter
	# -G: background color
	# -W: plot outline
	gmt plot "$REGION" "$PROJ" -Sc0.15c -Gred2 -W < "$TEMP"

	rm -f "$TEMP"
}

plot_eurostat_nuts() {
	data="$1"
	field_nuts="$2"
	field_data="$3"

	# NUTS-3 geometry: https://gisco-services.ec.europa.eu/distribution/v2/nuts/nuts-2021-files.html
	perl -MJSON -le '
		# read input
		while (<>) {
			# skip first line of population density dataset
			$. == 1 and next;

			# first file (population density): split fields, extract NUTS ID and data value
			# second file (geometry of NUTS regions): decode JSON
			!$f and (@F = split /[\t,]/ and $data{$F['"$field_nuts"']} = $F['"$field_data"']) or $nuts = decode_json($_);

			# switch datasets if first file has been read
			$/ = undef, $f++ if eof;
		}

		# iterate over NUTS regions
		for $nut (@{$nuts->{features}}) {

			$i = 0;

			# for each polygon:
			for (@{$nut->{geometry}{coordinates}}) {

				$id = $nut->{properties}{NUTS_ID};

				# use NUTS-3 data; if not available, then try NUTS-2 and NUTS-1 (= remove last characters)
				$val = defined $data{$id} ?
						$data{$id} :
					defined $data{$id =~ s/.$//r} ?
						$data{$id =~ s/.$//r} :
					defined $data{$id =~ s/..$//r} ?
						$data{$id =~ s/..$//r} :
						"nan";

				# write separator, followed by population density value (and debug comments)
				print "> -Z$val\t# $id, #$i";

				# write coordinate pairs; some regions have an additional array
				# around the coordinates, thus the case distinction
				print "@$_" for ($_->[0][0][0] ? @{$_->[0]} : @$_);

				$i++;
			}
		}
	' "$data" "$DATADIR/NUTS_RG_03M_2021_4326_LEVL_3.geojson" |
	gmt plot "$REGION" "$PROJ" -C -L
}


###############################################################################
# MAP 1:
# Velomobile dealers in central Europe

FILE='map_velomobile-dealers'
gmt begin "$FILE" pdf

# -A: min. feature size [km^2] (e.g. lakes), min level (0 = sea), max level (2 = lakes)
# -G: land in darkgrey
plot_land_sea -A20/0/1 -Gdarkgrey

plot_velomobile_dealers

# Write file
gmt end

# Convert to SVG
pdftocairo -svg "$FILE.pdf"



###############################################################################
# MAP 2:
# Velomobile users, with topography gradient

FILE='map_velomobile-users_topo-gradient'
gmt begin "$FILE" pdf

# Create topography grid for Europe in JPEG2000 format:
# gmt grdcut -R1/45.2/20/60+r @earth_relief_15s -Gearth_relief_15s_europe.jp2=gd:JP2OpenJPEG+cQUALITY=100+cREVERSIBLE=YES+cYCBCR420=NO

# Compute total derivative of elevation:
# -M: use meters as x,y units => derivative is m/m
# DUP: copy elevation => 2x elevation on stack
# DDX: compute derivative d/dx
# ABS: absolute value of the derivative
# EXCH: exchange data on stack => elevation copy on top, x derivative below
# DDY ABS: same for y direction
# ADD: add both derivatives on stack
gmt grdmath -M -I1m "$DATADIR/earth_relief_15s_europe.jp2" DUP DDX ABS EXCH DDY ABS ADD = "$DATADIR/earth_relief_total-derivative.grd"

# Create color table:
# -T: min/max
# -C: source color table, or colors
#     => range 0% to 10% with 5 colors = 2.5% per color
#     => source: mby.cpt (http://soliton.vm.bytemark.co.uk/pub/cpt-city/mby/index.html)
# -D: set colors for below/above range to the min/max colors
# -Z: continuous color table
gmt makecpt -Z -D -C0/97/71,16/123/48,232/214/125,163/68/0,130/30/30 -T0/0.1

# Plot grid
# -E: set resolution in dpi
gmt grdimage "$REGION" "$PROJ" -E100 "$DATADIR/earth_relief_total-derivative.grd"

# -A: lakes >= 0 km^2, up to level 2
# -I: rivers
plot_land_sea -A0/0/2 -Ir/thinner,royalblue4

plot_velomobile_drivers

# Plot color bar:
# -Dj: position
#   * TL: top left
#   * +w3c/0.3c: 3 cm long, 0.3 cm wide
#   * +v: vertical
#   * +o0.5c/0.5c: shift 0.5 cm left and 0.5 cm down
# -B:
#   * a: print annotation
#   * f: print also minor ticks
#   * +u"%": unit = percent
# -W100: multiply values by 100 to get percent
# -F: frame
#   * +pdefault: pen width = default
#   * +gwhite: white background
gmt colorbar "$REGION" "$PROJ" -DjTL+w3c/0.3c+v+o0.5c/0.5c -Baf+u"%" -W100 -F+pdefault+gwhite --FONT_ANNOT_PRIMARY=8p

# Write file
gmt end

# Convert to SVG and PNG
pdftocairo -svg "$FILE.pdf"



###############################################################################
# MAP 3:
# Velomobile drivers and population density

FILE='map_velomobile-users_population-density'
gmt begin "$FILE" pdf

# color scale: between 0 and 1000 people/km^2; use 0.1–1 from the input color scale
gmt makecpt -Z -Cinferno -D -T0/1000 -G0.1/1

# population density: https://ec.europa.eu/eurostat/estat-navtree-portlet-prod/BulkDownloadListing?dir=data&sort=1&sort=2&start=d
# NUTS ID is 2nd value, population density of 2019 is 4th value
plot_eurostat_nuts "$DATADIR/demo_r_d3dens.tsv" 1 3

# -A: min. feature size [km^2] (e.g. lakes), min level (0 = sea), max level (1 = coast)
# -G: land in darkgrey
plot_land_sea -A20/0/1

plot_velomobile_drivers

# Write file
gmt end

# Convert to SVG
pdftocairo -svg "$FILE.pdf"



###############################################################################
# MAP 4:
# Velomobile drivers and income

FILE='map_velomobile-users_income'
gmt begin "$FILE" pdf

# color scale: between 10000 and 25000 EUR; use 0.1–1 from the input color scale
gmt makecpt -Z -Cinferno -D -T10000/25000 -G0.1/1

# household income: https://ec.europa.eu/eurostat/de/web/products-datasets/-/NAMA_10R_2HHINC
# NUTS ID is 4nd value, income of 2019 is 6th value
plot_eurostat_nuts "$DATADIR/nama_10r_2hhinc.tsv" 3 5

# -A: min. feature size [km^2] (e.g. lakes), min level (0 = sea), max level (1 = coast)
# -G: land in darkgrey
plot_land_sea -A20/0/1

plot_velomobile_drivers

# Plot color bar:
# -Dj: position
#   * TL: top left
#   * +w3c/0.3c: 3 cm long, 0.3 cm wide
#   * +v: vertical
#   * +o0.5c/0.5c: shift 0.5 cm left and 0.5 cm down
# -B:
#   * a: print annotation
#   * f: print also minor ticks
#   * +u" EUR": unit
# -F: frame
#   * +c: clearance left/right/bottom/top
#   * +pdefault: pen width = default
#   * +gwhite: white background
gmt colorbar "$REGION" "$PROJ" -DjTL+w3c/0.3c+v+o0.5c/0.5c -Baf+u" EUR" -F+c0.15c/0.55c/0.15c/0.15c+pdefault+gwhite --FONT_ANNOT_PRIMARY=8p

# Write file
gmt end

# Convert to SVG
pdftocairo -svg "$FILE.pdf"
