Velomobile Knowledge Base
=========================

Description
-----------

This is a FAQ about Velomobiles, derived from the discussions in the [Velomobile forum](https://velomobilforum.de/). Cooperation desired!

*Deutsche Version: [siehe hier](README.md)*

**Read velomobile knowledge base**:

* **HTML:** [English](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge.html); [German](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen.html)
* **PDF:** [English](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge.pdf); [German](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen.pdf)
* **PDF print version:** [English](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge_print.pdf); [German](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen_print.pdf)
* **EPUB:** [English](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge_raster.epub) ([with SVG](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge.epub)); [German](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen_raster.epub) ([with SVG](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen.epub))
* **MOBI:** [English](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge.mobi); [German](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen.mobi)
* **Plain text:** [English](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge.txt); [German](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen.txt)

**Model comparison:** [VM slider](https://cmoder.gitlab.io/velomobil-grundwissen/vm-slider/vm-slider.html)

There is now a **printed German version**:

* 2nd edition: ISBN [978-3-756535-33-0](https://www.isbn.de/suche/?buecher=9783756535330); as of: Commit 513a28f; available e.g. [at da store](https://www.dastore.biz/product-page/velomobil-grundwissen) or [at epubli](https://www.epubli.de//shop/buch/Velomobil-Grundwissen-Christoph-Moder-9783756535330/130276).
* 1st edition: ISBN [978-3-754971-66-6](https://www.isbn.de/suche/?buecher=9783754971666); as of: Commit 2f238e1.

Changelog
---------
* Version 3.0: 2nd edition German print version, German chapters “Rechtliches” and “Berechnung”, datasets updated, index reworked
* Version 2.4: German print version, many corrections in English version
* Version 2.3: maps with Generic Mapping Tools, cover image (LaTeX, EPUB), Sphinx 4.0 + docutils 0.17
* Version 2.2: velomobile family tree, VM slider complemented, aerodynamics and lateral stability reworked, EPUB repaired, index reworked
* Version 2.1: many new diagrams, more data in VM slider
* Version 2.0: English language version, VM slider, newest Sphinx version
* Version 1.3: first graphics, EPUB with CSS, chapters sorted and divided
* Version 1.2: EPUB repaired and keyword index added
* Version 1.1: Glossary and index added
* Version 1.0: content largely complete
* Version 0.9: Versioning works, PDF generation works
* Version 0.5: first commits

Compile from the source code yourself
-------------------------------------
This project requires [Sphinx](http://www.sphinx-doc.org/), if this
is installed, it should be sufficient to use *make* to produce the output formats.
Since version 1.3 other language versions exist; thus, the language must
be given in an environment variable:

```sh
 $ make -e LANG=en html
 $ make -e LANG=en latexpdf
 $ make -e LANG=en epub
 $ make -e LANG=en text
```

=> The result is then in the subdirectory *_build*.

For full functionality, some additional software is required, e.g.
*LaTex* (e.g. *TeX Live*), *Python 3*, *Perl*, *Matplotlib*, *librsvg2*,
*Generic Mapping Tools*, *Imagemagick*, *Ghostscript*, *Calibre* and
the Sphinx extensions *alabaster* und *svg2pdfconverter*. In addition,
some post-processing is done on *Gitlab*; see *.gitlab-ci.yml*.

Cooperation
-----------

* via pull request (requires Gitlab account)
* in the Velomobil forum [in this thread](https://www.velomobilforum.de/forum/index.php?threads/faq-velomobil-grundwissen.57837/) (requires an account there)
* by email to the authors

Contributors and License
------------------------

See text, section [*Contributors and Sources*](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge.html#contributors-and-sources).

Unless otherwise noted, everything is under the license [*Creative Commons CC-BY*](https://creativecommons.org/licenses/by/4.0/deed.en) – in short: Anyone can use the text as desired, but must keep the names of the authors/translators.
