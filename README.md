Velomobil-Grundwissen
=====================

Beschreibung
------------
Dies ist eine FAQ über Velomobile, abgeleitet aus den Diskussionen aus dem
[Velomobil-Forum](https://velomobilforum.de/). Mitarbeit erwünscht!

*English version: [see here](README_en.md)*

**Velomobil-Grundwissen lesen**:

* **HTML:** [deutsch](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen.html), [englisch](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge.html)
* **PDF:** [deutsch](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen.pdf), [englisch](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge.pdf)
* **PDF-Druckversion:** [deutsch](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen_print.pdf), [englisch](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge_print.pdf)
* **EPUB:** [deutsch](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen_raster.epub) ([mit SVG](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen.epub)), [englisch](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge_raster.epub) ([mit SVG](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge.epub))
* **MOBI:** [deutsch](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen.mobi), [englisch](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge.mobi)
* **Klartext:** [deutsch](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen.txt), [englisch](https://cmoder.gitlab.io/velomobil-grundwissen/velomobile_knowledge.txt)

**Modellvergleich:** [VM-Slider](https://cmoder.gitlab.io/velomobil-grundwissen/vm-slider/vm-slider.html)

Es gibt inzwischen auch eine **Druckversion**:

* 2. Auflage: ISBN [978-3-756535-33-0](https://www.isbn.de/suche/?buecher=9783756535330); Stand: Commit 513a28f; z.B. [bei da store](https://www.dastore.biz/product-page/velomobil-grundwissen) oder auch [bei epubli](https://www.epubli.de//shop/buch/Velomobil-Grundwissen-Christoph-Moder-9783756535330/130276) zu kaufen.
* 1. Auflage: ISBN [978-3-754971-66-6](https://www.isbn.de/suche/?buecher=9783754971666); Stand: Commit 2f238e1.

Changelog
---------
* Version 3.0: 2. Auflage deutsche Druckversion, Kapitel „Rechtliches“ + „Berechnung“, Datensätze aktualisiert, Stichwörter überarbeitet
* Version 2.4: deutsche Druckversion, viele Korrekturen in englischer Version
* Version 2.3: Karten mit Generic Mapping Tools, Titelbild (LaTeX, EPUB), Sphinx 4.0 + docutils 0.17
* Version 2.2: VM-Stammbaum, VM-Slider ergänzt, Aerodynamik und Kippstabilität überarbeitet, EPUB repariert, Stichwörter überarbeitet
* Version 2.1: viele neue Diagramme, mehr Daten im VM-Slider
* Version 2.0: englischsprachige Version, VM-Slider, neueste Sphinx-Version
* Version 1.4: Diagramme mit Matplotlib
* Version 1.3: erste Grafiken, EPUB mit CSS, Kapitel umsortiert und unterteilt
* Version 1.2: EPUB repariert, und ebenfalls Stichwortverzeichnis hinzugefügt
* Version 1.1: Glossar und Stichwortverzeichnis hinzugefügt
* Version 1.0: Inhalt weitgehend vollständig
* Version 0.9: Versionierung funktioniert, PDF-Generierung funktioniert
* Version 0.5: erste Commits

Quellcode selber kompilieren
----------------------------
Dieses Projekt erfordert [Sphinx](http://www.sphinx-doc.org/); wenn dieses
installiert ist, sollte es ausreichen, mittels *make* die Ausgabeformate zu
erzeugen. Seit Version 1.3 sind mehrere Sprachversionen möglich; entsprechend
muss die Sprachversion in einer Umgebungsvariablen angegeben werden:

```sh
 $ make -e LANG=de html
 $ make -e LANG=de latexpdf
 $ make -e LANG=de epub
 $ make -e LANG=de text
```

=> Das Ergebnis ist dann im Unterverzeichnis *_build*.

Für die volle Funktionalität ist noch einige zusätzliche Software nötig,
beispielsweisweise *LaTex* (z.B. *TeX Live*), *Python 3*, *Perl*, *Matplotlib*,
*librsvg2*, *Generic Mapping Tools*, *Imagemagick*, *Ghostscript*, *Calibre*
und die Sphinx-Erweiterungen *alabaster* und *svg2pdfconverter*.
Außerdem werden auf *Gitlab* nach der Erzeugung noch diverse Nachbearbeitungen
gemacht, siehe *.gitlab-ci.yml*.

Mitarbeit
---------
* per Pull-Request (erfordert Gitlab-Account)
* im Velomobil-Forum [in diesem Thread](https://www.velomobilforum.de/forum/index.php?threads/faq-velomobil-grundwissen.57837/) (erfordert dortigen Account)
* per Mail an die Autoren

Mitwirkende und Lizenz
----------------------
Siehe im Text, Abschnitt [*Mitwirkende und Quellen*](https://cmoder.gitlab.io/velomobil-grundwissen/Velomobil-Grundwissen.html#mitwirkende-und-quellen).

Soweit nicht anders angegeben, steht alles unter der Lizenz
[*Creative Commons CC-BY*](https://creativecommons.org/licenses/by/4.0/deed.de)
– kurz gesagt: Jeder darf den Text beliebig weiterverwenden, muss aber die Namen
der Autoren nennen.
