#!/bin/bash

# DESCRIPTION:
# Downloads the user list from velomobilforum.de and uses geocoding to get the coordinates.

# USAGE:
# $ ./vmforum_download_memberlist.sh > list.csv


# Login data to velomobilforum.de
# URL encoded data, e.g. email address as "user%40domain.org"
USERNAME='my_username'
PASSWORD='my_password'

# Email address, for Nominatim bulk requests
EMAIL='user%40domain.org'


WGET_OPTS='--load-cookies cookies.txt --save-cookies cookies.txt --keep-session-cookies'

# Terminate script after error
set -e

# Login to velomobilforum.de
TOKEN="$(wget -q -O -  $WGET_OPTS 'https://www.velomobilforum.de/forum/index.php?login/login' | sed -n -e 's!.*name="_xfToken" value="\([^"]\+\)".*!\1!p; T; q')"
wget -q -O /dev/null $WGET_OPTS --post-data "login=$USERNAME&password=$PASSWORD&remember=1&_xfRedirect=https%3A%2F%2Fwww.velomobilforum.de%2Fforum%2Findex.php&_xfToken=$TOKEN" 'https://www.velomobilforum.de/forum/index.php?login%2Flogin'


# Get number of pages
PAGES="$(wget -q -O - $WGET_OPTS 'https://www.velomobilforum.de/forum/index.php?members/list/' |
	sed -n -e 's!.*<li class="pageNav-page "><a href="/forum/index.php?members/list/&amp;page=\([0-9]\+\)">.*!\1!p')"


# Iterate over pages of member list
for ((i = 1; i <= $PAGES; i++)); do
	wget -q -O - $WGET_OPTS "https://www.velomobilforum.de/forum/index.php?members/list/&page=$i"
	echo "Downloading member list page $i/$PAGES ..." >&2
done |

perl -nle '
	use LWP::Simple;
	use URI::Escape;
	use JSON;
	use utf8;

	BEGIN {
		%countries = (
			"D" => "de",
			"DE" => "de",
			"Deutschland" => "de",
			"A" => "at",
			"Austria" => "at",
			"Österreich" => "at",
			"CH" => "ch",
			"Schweiz" => "ch",
			"I" => "it",
			"Italien" => "it",
			"Italia" => "it",
			"NL" => "nl",
			"Niederlande" => "nl",
			"Netherlands" => "nl",
			"Holland" => "nl",
			"B" => "be",
			"Belgien" => "be",
			"F" => "fr",
			"FR" => "fr",
			"Frankreich" => "fr",
			"France" => "fr",
			"UK" => "gb",
			"U.K." => "gb",
			"GB" => "gb",
			"Großbritannien" => "gb",
			"England" => "gb",
			"Schottland" => "gb",
			"DK" => "dk",
			"Dänemark" => "dk",
			"Denmark" => "dk",
			"N" => "no",
			"Norwegen" => "no",
			"Norge" => "no",
			"Norway" => "no",
			"S" => "se",
			"Schweden" => "se",
			"Sverige" => "se",
			"Sweden" => "se",
			"FIN" => "fi",
			"Finnland" => "fi",
			"Finland" => "fi",
			"Polen" => "pl",
			"Spanien" => "es",
			"Spain" => "es",
			"SK" => "sk",
			"Slowakei" => "sk",
			"Slowenien" => "si",
			"USA" => "us",
			"CAN" => "ca",
			"Kanada" => "ca",
			"Canada" => "ca",
			"Australien" => "au",
			"Australia" => "au",
		);

		# regex = all country names with at least 2 letters and not frequent character combinations
		$country_regex = join "|", grep { /../ and !/DE|CH|FR|NL|FIN/ } keys %countries;
	}

	# Get data from the member lists:
	/<h3[^>]+><a href="[^>]+ data-user-id="([^"]+)">(?:<.+?>)?([^<>]+)(?:<[^<>]+>)?<\/a>/ and $v{uid} = 0 + $1, $v{name} = $2;
	/<div class="contentRow-lesser" dir="auto"\s*>\s*(\d+)\s*<span role="presentation" aria-hidden="true">&middot;<\/span>/ and $v{age} = $1;
	/<div .*Aus <a href="\/forum\/index.php\?misc\/location-info&location=[^"]+" class="u-concealed" target="_blank" rel="nofollow noreferrer">([^<>]+)<\/a><\/div>/ and $v{Ort} = $1;
	/<dt>([^<>]+)<\/dt>/ and $prop = $1;
	/<dd>([^<>]+)<\/dd>/ and $v{$prop} = 0 + ($1 =~ s/\.//gr);

	# At the end of each member entry, get the user information page and do the geocoding.
	if (/^	<\/div>/ and $v{uid}) {

		$userpage = `wget -q -O - '"$WGET_OPTS"' "https://www.velomobilforum.de/forum/index.php?members/$v{uid}/about"`;

		# Get information (bike type etc.) from user page, remove HTML tags
		($v{$1} = $2) =~ s!<br( )/>|<[^<>]+?>!$1!g while ($userpage =~ m!<dt>(Velomobil|Liegerad|Trike|Gender|sonstige Fahrzeuge/Bemerkungen)</dt>\s*<dd>\s*((?:[^<]|<(?\!/dd>))+)\s*</dd>!sg);

		# If a location is given, retrieve its coordinates
		if ($v{Ort}) {

			%address = ("q" => $v{Ort}, "countrycodes" => "de,at,ch");

			# assume 4 or 5 digits = postcode, and capital letters before = country code
			$v{Ort} =~ /(?:([A-Z]+)\s*-\s*)?(\d{4,5})/ and $address{postalcode} = $2 and $1 and $address{country} = $countries{$1};

			# search for country names, and convert them to country codes
			$v{Ort} =~ /($country_regex)/i and $address{country} = $countries{$1};

			# if a 5-digit postcode is found, assume the country is Germany (unless defined otherwise)
			!$address{country} and $address{postalcode} =~ /\d{5}/ and $address{country} = "de";

			# create town name by removing all other known parts:
			$address{city} = $v{Ort}
				# remove postcode
				=~ s/(?:[A-Z]+\s*-\s*)?\d{4,5}//r
				# remove country name
				=~ s/$country_regex//gri
				# remove punctuation
				=~ s/[,.!?*<>]//gr
				# remove spaces at beginning or end
				=~ s/^\s+//r
				=~ s/\s+$//r;

			# If at least country or postal code are defined, search by all available items; otherwise, do a free-form search.
			$v{addrtype} = ($address{country} or $address{postalcode}) ? "country_postalcode" : "city_dach";

			# Try the search; if the parsed search fails, retry a free-form search.
			while (1) {
				$url_params = join "",
					map { "&$_=" . uri_escape($address{$_}) }
					grep { $address{$_} } (
						$v{addrtype} eq "country_postalcode" ?
						("country", "postalcode", "city") :
						$v{addrtype} eq "city_dach" ?
						("city", "countrycodes") :
						("q")
					);

				# Perform Nominatim query
				do {
					$json = get("http://nominatim.openstreetmap.org/search?format=json&dedupe=1&limit=1${url_params}&email='"$EMAIL"'");
				} until ($json);
				$c = decode_json($json);
				$c->[0] and map { $v{$_} = 0 + $c->[0]{$_} } ("lat", "lon") and $v{radius} = int(($c->[0]{boundingbox}[1] - $c->[0]{boundingbox}[0]) * 60 * 1852 / 2 + 0.5);

				# Nominatim allows only 1 request per second
				sleep 1;

				# Terminate search if a coordinate has been found or if all possibilities have been exhausted.
				last if $v{lat} or $v{addrtype} eq "free";

				# Switch address type one step further:
				# * if country_postalcode, then search for city in D/A/CH
				# * if city search in D/A/CH, then worldwide
				$v{addrtype} = {"country_postalcode" => "city_dach", "city_dach" => "free"}->{$v{addrtype}};
			}

		}

		# Output the data
		print join "\t", map { $v{$_} =~ s/[\r\n\t]//gr } ("uid", "name", "Ort", "lat", "lon", "radius", "Velomobil", "Liegerad", "Trike", "sonstige Fahrzeuge/Bemerkungen", "Beiträge", "Punkte für Reaktionen", "age", "Gender", "Alben", "Medien", "addrtype");

		# delete variables at the end
		%v = ();
	}
	'

# Delete cookies file
rm -f cookies.txt
